/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab2.Trwalosc;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author student
 */
@Entity
@Table(name = "CROSSWORDS")
@NamedQueries({
    @NamedQuery(name = "Crosswords.findAll", query = "SELECT c FROM Crosswords c"),
    @NamedQuery(name = "Crosswords.findByCrosswordId", query = "SELECT c FROM Crosswords c WHERE c.crosswordId = :crosswordId"),
    @NamedQuery(name = "Crosswords.findByName", query = "SELECT c FROM Crosswords c WHERE c.name = :name"),
    @NamedQuery(name = "Crosswords.findByPricePln", query = "SELECT c FROM Crosswords c WHERE c.pricePln = :pricePln"),
    @NamedQuery(name = "Crosswords.findByPages", query = "SELECT c FROM Crosswords c WHERE c.pages = :pages"),
    @NamedQuery(name = "Crosswords.findByFormat", query = "SELECT c FROM Crosswords c WHERE c.format = :format"),
    @NamedQuery(name = "Crosswords.findByEditorName", query = "SELECT c FROM Crosswords c WHERE c.editorName = :editorName"),
    @NamedQuery(name = "Crosswords.findByHasOtherPuzzles", query = "SELECT c FROM Crosswords c WHERE c.hasOtherPuzzles = :hasOtherPuzzles")})
public class Crosswords implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CROSSWORD_ID")
    private Integer crosswordId;
    @Column(name = "NAME")
    private String name;
    @Column(name = "PRICE_PLN")
    private Integer pricePln;
    @Column(name = "PAGES")
    private Integer pages;
    @Column(name = "FORMAT")
    private String format;
    @Column(name = "EDITOR_NAME")
    private String editorName;
    @Column(name = "HAS_OTHER_PUZZLES")
    private Boolean hasOtherPuzzles;

    public Crosswords() {
    }

    public Crosswords(Integer crosswordId) {
        this.crosswordId = crosswordId;
    }

    public Integer getCrosswordId() {
        return crosswordId;
    }

    public void setCrosswordId(Integer crosswordId) {
        this.crosswordId = crosswordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPricePln() {
        return pricePln;
    }

    public void setPricePln(Integer pricePln) {
        this.pricePln = pricePln;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getEditorName() {
        return editorName;
    }

    public void setEditorName(String editorName) {
        this.editorName = editorName;
    }

    public Boolean getHasOtherPuzzles() {
        return hasOtherPuzzles;
    }

    public void setHasOtherPuzzles(Boolean hasOtherPuzzles) {
        this.hasOtherPuzzles = hasOtherPuzzles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (crosswordId != null ? crosswordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Crosswords)) {
            return false;
        }
        Crosswords other = (Crosswords) object;
        if ((this.crosswordId == null && other.crosswordId != null) || (this.crosswordId != null && !this.crosswordId.equals(other.crosswordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Lab2.Trwalosc.Crosswords[ crosswordId=" + crosswordId + " ]";
    }
    
}
